export const config = {
    apiUrl: process.env.REACT_APP_API_URL,
    gameLevels: [
      {
        value: 'easyMode',
        name: 'Easy'
      },
      {
        value: 'normalMode',
        name: 'Normal'
      },
      {
        value: 'hardMode',
        name: 'Hard'
      }
    ]
  };
  