import React from 'react';
import './App.css';
import 'antd/dist/antd.css';
import SettingContainer from './containers/SettingContainer';
import FieldContainer from './containers/FieldContainer';
import HistoryContainer from './containers/HistoryContainer';
import { Layout } from 'antd';

const { Header, Sider, Content } = Layout;

const App = () => {
  return (
    <Layout className="App">
      <Header className="app-header">
        <SettingContainer />
      </Header>
      <Layout>
        <Content className="app-content">
          <FieldContainer />
        </Content>
        <Sider className="app-side">
          <HistoryContainer />
        </Sider>
      </Layout>
    </Layout>
  );
};

export default App;