import axios from 'axios';
import { config } from '../../config/config';

export default class BaseApiService {
  handleResponse = response => {
    return response.data;
  };

  handleError = error => {
    if (error.response)
      return {
        error: error.response.data.message || error.response.data.error
      };
    else return { error: error };
  };

  request = ({ method, url, params, data }) => {

    return axios({
      baseURL: config.apiUrl,
      timeout: 4000,
      maxContentLength: 5000,
      method,
      url,
      params,
      data
    }).then(this.handleResponse, this.handleError);
  };
}
