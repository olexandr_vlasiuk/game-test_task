import { GameApiService } from './game';

class ApiService {
  constructor() {
    this.game = new GameApiService();
  }
}

export const Api = new ApiService();
