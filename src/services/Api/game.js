import BaseApiService from './base';

export class GameApiService extends BaseApiService {
  
  getWinners() {
    return this.request({
      method: 'GET',
      url: 'winners'
    });
  }
  
  getGameSettings() {
    return this.request({
      method: 'GET',
      url: 'game-settings'
    });
  }

  sendToHistory(data) {
    return this.request({
      method: 'POST',
      url: 'winners',
      data,
    });
  }
}