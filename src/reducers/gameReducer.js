import { handleActions } from 'redux-actions';
import { 
  SET_ACTIVE_MODE_SETTINGS,
  ON_ACTIVE_MODE_CHANGE,
  ON_ACTIVE_CELL_CHANGE,
  ON_HISTORY_LOAD,
  ON_USER_NAME_CHANGE,
  ON_GREEN_CELL_ADD,
  ON_RED_CELL_ADD,
  IS_GAME_GOING,
  SHOW_WINNER,
  ON_RESET
} from '../actions';

const initialState = {
  activeMode: 'easyMode',
  activeModeSettings: {},
  userName: 'Gamer',
  activeCell: null,
  greenCells: [],
  redCells: [],
  historyInfo: [],
  isGameGoing: false,
  winnerName: null,
  showWinner: false
};

export default handleActions(
  {
    [SET_ACTIVE_MODE_SETTINGS]: (state, { payload: { settings }}) => {
      return {
        ...state,
        activeModeSettings: settings
      };
    },
    [ON_ACTIVE_MODE_CHANGE]: (state, { payload: { value }}) => {
      return {
        ...state,
        activeMode: value
      };
    },
    [ON_ACTIVE_CELL_CHANGE]: (state, { payload: { number }}) => {
      return {
        ...state,
        activeCell: number
      };
    },
    [ON_HISTORY_LOAD]: (state, { payload: { data }}) => {
      return {
        ...state,
        historyInfo: [...data]
      };
    },
    [ON_USER_NAME_CHANGE]: (state, { payload: { value }}) => {
      return {
        ...state,
        userName: value
      };
    },
    [ON_GREEN_CELL_ADD]: (state, { payload: { newCell }}) => {
      return {
        ...state,
        greenCells: [...state.greenCells, newCell]
      };
    },
    [ON_RED_CELL_ADD]: (state, { payload: { newCell }}) => {
      return {
        ...state,
        redCells: [...state.redCells, newCell]
      };
    },
    [IS_GAME_GOING]: (state, { payload: { bool }}) => {
      return {
        ...state,
        isGameGoing: bool
      };
    },
    [SHOW_WINNER]: (state, { payload: { bool }}) => {
      return {
        ...state,
        showWinner: bool
      };
    },
    [ON_RESET]: (state, { payload: { 
        activeMode,
        activeModeSettings,
        historyInfo,
        winner,
        showWinner
      }}) => {
      return {
        activeMode: activeMode,
        activeModeSettings: activeModeSettings,
        userName: state.userName,
        activeCell: null,
        greenCells: [],
        redCells: [],
        historyInfo: historyInfo,
        isGameGoing: false,
        winnerName: winner,
        showWinner
      }
    },
  },
  initialState
);
