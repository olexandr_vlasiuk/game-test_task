import React from 'react';
import Field from '../components/field';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  onRedCellAdd,
  isGameGoing,
  onReset,
  onHistoryLoad,
  onGreenCellAdd
} from '../redux/actions';
import { Api } from '../services/Api';

class FieldContainer extends React.Component {

  componentDidUpdate() {
    // End of game
    if (this.props.redCells.length === Math.pow(this.props.activeModeSettings.field, 2)) {
      // Find winner
      if (this.props.greenCells.length > (this.props.redCells.length - this.props.greenCells.length))
        this.onGameFinish(this.props.userName, this.props.activeMode); // Set current user as winner
      else
        this.onGameFinish('Computer', this.props.activeMode); // Set computer as winner
    }
  }

  addZeroBefore = (num) => {
    return ('0' + num).slice(-2)
  }

  onGameFinish = async (winner, activeMode) => {
    this.props.gameActions.onRedCellAdd(0);
    this.props.gameActions.isGameGoing(false);
    
    const date = new Date();
    await Api.game.sendToHistory({
      winner: `${winner || 'Anonymous'}`,
      date: `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()} ${this.addZeroBefore(date.getHours())}:${this.addZeroBefore(date.getMinutes())}`
    });

    const historyData = await Api.game.getWinners();
    const gameModeDate = await Api.game.getGameSettings();
    this.props.gameActions.onReset(activeMode, gameModeDate[activeMode], historyData, winner || 'Anonymous', true);
  };

  render() {
    return (
      <Field
        {...this.props}
        onActiveCellClick={cellKey => this.props.gameActions.onGreenCellAdd(cellKey)}
      />
    );
  };
}

const mapStateToProps = ({ game }) => (
  {
    activeModeSettings: game.activeModeSettings,
    activeCell: game.activeCell,
    greenCells: game.greenCells,
    redCells: game.redCells,
    userName: game.userName,
    activeMode: game.activeMode
  }
);

const mapDispatchToProps = dispatch => ({
  gameActions: bindActionCreators({
    onRedCellAdd,
    isGameGoing,
    onReset,
    onHistoryLoad,
    onGreenCellAdd
  }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FieldContainer);