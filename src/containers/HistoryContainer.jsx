import React from 'react';
import History from '../components/history';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { onHistoryLoad } from '../redux/actions';
import { Api } from '../services/Api';

class HistoryContainer extends React.Component {
  
  componentDidMount() {
    Api.game.getWinners().then(data => 
      this.props.gameActions.onHistoryLoad(data));
  }

  render() {
    return <History {...this.props} />
  };
}

const mapStateToProps = ({ game }) => (
  {
    historyInfo: game.historyInfo
  }
);

const mapDispatchToProps = dispatch => ({
  gameActions: bindActionCreators({
    onHistoryLoad
  }, dispatch)
});

export default connect(
  mapStateToProps, 
  mapDispatchToProps
)(HistoryContainer);