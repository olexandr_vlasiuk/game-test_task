import React from 'react';
import Settings from '../components/gameSettings';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  setActiveModeSettings,
  onActiveModeChange,
  isGameGoing,
  onActiveCellChange,
  onRedCellAdd,
  onUserNameChange,
  showWinner
} from '../redux/actions';
import { Api } from '../services/Api';

class SettingContainer extends React.Component {

  state = {
    settings: []
  }

  componentDidMount() {
    Api.game.getGameSettings().then(settings =>
      this.setState({ settings }, () => this.props.gameActions.setActiveModeSettings(settings['easyMode']))
    );
  }

  onGameModeChange = gameMode => {
    this.props.gameActions.setActiveModeSettings(this.state.settings[gameMode]);
    this.props.gameActions.onActiveModeChange(gameMode);
  };

  onGameStart = () => {
    // Board size
    const max = Math.pow(this.props.activeModeSettings.field, 2);
    // Generating coordinates
    const numbers = [...Array(max).keys()];
    // Start game
    this.props.gameActions.isGameGoing(true);
    this.iterator(1, -1, numbers, max);
    this.props.gameActions.showWinner(false);
  };

  // Recursive function for generating moves and Computer response
  iterator = (i, redCell, numbers, max) => {
    setTimeout(() => {
      const random = Math.floor(Math.random() * Math.floor(numbers.length));
      this.props.gameActions.onActiveCellChange(numbers[random]);

      // Computer`s move
      if (redCell >= 0) this.props.gameActions.onRedCellAdd(redCell);

      // Next computers move
      redCell = numbers[random];

      numbers.splice(random, 1);
      // Begin new iteration
      if (i <= max) this.iterator(++i, redCell, numbers, max);
    }, this.props.activeModeSettings.delay);
  }

  render() {
    return (
      <Settings
        activeMode={this.props.activeMode}
        onGameModeChange={this.onGameModeChange}
        userName={this.props.userName}
        onUserNameChange={this.props.gameActions.onUserNameChange}
        onGameStart={this.onGameStart}
        isGameGoing={this.props.isGameGoing}
        winnerName={this.props.winnerName}
        showWinner={this.props.gameActions.showWinner}
        showWinnerBlock={this.props.showWinnerBlock}
      />
    );
  };
}

const mapStateToProps = ({ game }) => (
  {
    activeMode: game.activeMode,
    userName: game.userName,
    activeModeSettings: game.activeModeSettings,
    fieldKeys: game.fieldKeysArr,
    isGameGoing: game.isGameGoing,
    winnerName: game.winnerName,
    gamesPlayed: game.gamesPlayed,
    showWinnerBlock: game.showWinner
  }
);

const mapDispatchToProps = dispatch => ({
  gameActions: bindActionCreators({
    setActiveModeSettings,
    onActiveModeChange,
    isGameGoing,
    onActiveCellChange,
    onRedCellAdd,
    onUserNameChange,
    showWinner
  }, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingContainer);