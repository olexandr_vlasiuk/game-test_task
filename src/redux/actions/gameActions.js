import { createActions } from 'redux-actions';

const SET_ACTIVE_MODE_SETTINGS = "SET_ACTIVE_MODE_SETTINGS";
const ON_ACTIVE_MODE_CHANGE = "ON_ACTIVE_MODE_CHANGE";
const ON_ACTIVE_CELL_CHANGE = "ON_ACTIVE_CELL_CHANGE";
const ON_HISTORY_LOAD = "ON_HISTORY_LOAD";
const ON_USER_NAME_CHANGE = "ON_USER_NAME_CHANGE";
const ON_GREEN_CELL_ADD = "ON_GREEN_CELL_ADD";
const ON_RED_CELL_ADD = "ON_RED_CELL_ADD";
const IS_GAME_GOING = "IS_GAME_GOING";
const SHOW_WINNER = "SHOW_WINNER";
const ON_RESET = "ON_RESET";

const {
  setActiveModeSettings,
  onActiveModeChange,
  onActiveCellChange,
  onHistoryLoad,
  onUserNameChange,
  onGreenCellAdd,
  onRedCellAdd,
  isGameGoing,
  showWinner,
  onReset
} = createActions({
  [SET_ACTIVE_MODE_SETTINGS]: settings => ({ settings }),
  [ON_ACTIVE_MODE_CHANGE]: value => ({ value }),
  [ON_ACTIVE_CELL_CHANGE]: number => ({ number }),
  [ON_HISTORY_LOAD]: data => ({ data }),
  [ON_USER_NAME_CHANGE]: value => ({ value }),
  [ON_GREEN_CELL_ADD]: newCell => ({ newCell }),
  [ON_RED_CELL_ADD]: newCell => ({ newCell }),
  [IS_GAME_GOING]: bool => ({ bool }),
  [SHOW_WINNER]: bool => ({ bool }),
  [ON_RESET]: (activeMode, activeModeSettings, historyInfo, winner, showWinner) => ({ activeMode, activeModeSettings, historyInfo, winner, showWinner }),
});

export {
  setActiveModeSettings,
  SET_ACTIVE_MODE_SETTINGS,
  onActiveModeChange,
  ON_ACTIVE_MODE_CHANGE,
  onActiveCellChange,
  ON_ACTIVE_CELL_CHANGE,
  onHistoryLoad,
  ON_HISTORY_LOAD,
  onUserNameChange,
  ON_USER_NAME_CHANGE,
  onGreenCellAdd,
  ON_GREEN_CELL_ADD,
  onRedCellAdd,
  ON_RED_CELL_ADD,
  isGameGoing,
  IS_GAME_GOING,
  SHOW_WINNER,
  showWinner,
  onReset,
  ON_RESET
};
