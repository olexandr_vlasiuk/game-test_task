import React from 'react';
import { Col, Row } from 'antd';
import classes from './Field.module.css';

const boardSettings = {
  5: {
    span: 4,
    gameMode: 'easy'
  },
  10: {
    span: 2,
    gameMode: 'normal'
  },
  15: {
    span: 1,
    gameMode: 'hard'
  }
}

// Get prepared data for each cell for game board
const getCellData = (row, col, length, props) => {
  const cellKey = row === 0 ? col : col + (length * row);
  const { span, gameMode } = boardSettings[length];
  let onClick= () => { };
  let className = `${classes.col} ${classes[gameMode]}`;

  if ([...props.greenCells, ...props.redCells].includes(cellKey)) {
      className=`${classes.col} ${classes[gameMode]} ${props.greenCells.includes(cellKey) ? classes.green : classes.red}`
  } else if (cellKey === props.activeCell) {
      className=`${classes.col} ${classes[gameMode]} ${classes.blue}`
      onClick= () => { props.onActiveCellClick(cellKey) }
  }

  return {
    span,
    cellKey,
    className,
    onClick
  }
}

const Field = props => {
  if (!props.activeModeSettings.field) return <div />;

  const cells = [...Array(props.activeModeSettings.field).keys()];

  return (
    <div className={`${classes.field}`}>
      {
        cells.map(row => {
          return (
            <Row className={classes.row} key={row}>
              {
                cells.map(col => {
                  const { 
                    span,
                    cellKey,
                    className,
                    onClick
                  } = getCellData(row, col, cells.length, props);

                  return (
                    <Col
                      className={className}
                      key={cellKey}
                      span={span}
                      onClick={onClick}
                    />
                  );
                })
              }
            </Row>
          )
        })
      }
    </div>
  );
};

export default Field;