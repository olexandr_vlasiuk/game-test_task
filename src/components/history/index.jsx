import React from 'react';
import classes from './History.module.css';
import { List } from 'antd';

const History = props =>
  <div className={classes.winnerList}>
    <List
      itemLayout="horizontal"
      dataSource={props.historyInfo.reverse()}
      renderItem={item => (
        <List.Item>
          <List.Item.Meta
            title={item.winner}
            description={item.date}
          />
        </List.Item>
      )}
    />
  </div>

export default History;
