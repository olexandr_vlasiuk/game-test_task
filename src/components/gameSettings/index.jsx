import React from 'react';
import { Select, Input, Button, Row, Col, Icon } from 'antd';
import { config } from '../../config/config';

const { Option } = Select;

const Settings = props => (
  <Row>
    <Row type="flex" justify="space-between">
      <Col xs={10} md={8}>
        <span>Choose game mode: </span>
        <Select
          value={props.activeMode}
          onChange={(value) => { props.onGameModeChange(value) }}
          disabled={props.isGameGoing}
          style={{ minWidth: '100px' }}
        >
          {
            config.gameLevels.map((v, k) =>
              <Option key={`option-${k}`} value={v.value}>{v.name}</Option>
            )
          }
        </Select>
      </Col>
      <Col
        xs={8}
        md={5}
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        <Input
          placeholder="User name"
          value={props.userName}
          onChange={e => props.onUserNameChange(e.target.value)}
          addonBefore={<span>Your name: </span>}
          disabled={props.isGameGoing}
        />
      </Col>
      <Col xs={4} md={4}>
        <Button
          type="primary"
          size="large"
          disabled={props.isGameGoing}
          style={{ width: '100%' }}
          onClick={() => { props.onGameStart() }}
        >
          {props.winnerName === null ? 'Play' : 'Play again'}
        </Button>
      </Col>

    </Row>
    {
      props.showWinnerBlock &&
      <Row className="winnerName">
        <Col>
          <span>The winner is {props.winnerName}</span>
          <Icon type="close" style={{
            position: 'absolute',
            right: 0,
            margin: '5px',
            fontSize: 'initial',
            cursor: 'pointer'
          }} onClick={() => props.showWinner(false)}/>
        </Col>
      </Row>
    }
  </Row>
);

export default Settings;